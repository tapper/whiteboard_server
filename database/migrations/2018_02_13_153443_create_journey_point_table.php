<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJourneyPointTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journey_point', function (Blueprint $table) {
            $table->integer('journey_id')->unsigned()->index();
            $table->foreign('journey_id')->references('id')->on('journeys')->onDelete('cascade');
            $table->integer('point_id')->unsigned()->index();
            $table->foreign('point_id')->references('id')->on('points')->onDelete('cascade');
            $table->primary(['journey_id', 'point_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('journey_point');
    }
}
