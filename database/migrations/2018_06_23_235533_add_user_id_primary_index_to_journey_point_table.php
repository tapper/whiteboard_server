<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdPrimaryIndexToJourneyPointTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('journey_point', function (Blueprint $table) {

            $sm = Schema::getConnection()->getDoctrineSchemaManager();
            $indexesFound = $sm->listTableIndexes('journey_point');

            if(array_key_exists('journey_point_journey_id_point_id_primary', $indexesFound))
                $table->dropPrimary(['journey_id', 'point_id']);

            if(array_key_exists('primary', $indexesFound))
                $table->dropPrimary('primary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('journey_point', function (Blueprint $table) {
            $table->primary(['journey_id', 'point_id']);
        });
    }
}
