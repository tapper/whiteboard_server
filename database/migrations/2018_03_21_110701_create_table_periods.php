<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePeriods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('touchpoint_periods', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_journey_id')->unsigned()->index();
            $table->foreign('user_journey_id')->references('id')->on('user_journeys')->onDelete('cascade');
            $table->integer('touchpoint_id')->unsigned()->index()->nullable();
            $table->foreign('touchpoint_id')->references('id')->on('touchpoints')->onDelete('cascade');
            $table->timestamp('started_at')->useCurrent();
            $table->timestamp('finished_at')->nullable();
            $table->integer('duration')->comment('in seconds')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('touchpoint_periods');
    }
}
