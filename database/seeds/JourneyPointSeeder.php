<?php

use Illuminate\Database\Seeder;

class JourneyPointSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $journeys = \App\Models\Journey::all();
        $points = \App\Models\Point::all();

        foreach ($journeys as $journey){
            foreach ($points as $point){
                $journey->points()->attach($point, ['created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()]);
            }
        }
    }
}
