<?php

use Illuminate\Database\Seeder;

class PointsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $p1 = new \App\Models\Point();
        $p1->translateOrNew('en')->title = 'Home';
        $p1->translateOrNew('he')->title = 'בית';
        $p1->color = '#27CC6A';
        $p1->save();

        $p2 = new \App\Models\Point();
        $p2->translateOrNew('en')->title = 'Office';
        $p2->translateOrNew('he')->title = 'משרד';
        $p2->color = '#FED08A';
        $p2->save();

        $p3 = new \App\Models\Point();
        $p3->translateOrNew('en')->title = 'Car';
        $p3->translateOrNew('he')->title = 'רכב';
        $p3->color = '#B79780';
        $p3->save();

        $p4 = new \App\Models\Point();
        $p4->translateOrNew('en')->title = 'Walk In';
        $p4->translateOrNew('he')->title = 'הליכה';
        $p4->color = '#1CA8FC';
        $p4->save();

        $p5 = new \App\Models\Point();
        $p5->translateOrNew('en')->title = 'Line';
        $p5->translateOrNew('he')->title = 'תור';
        $p5->color = '#5A6977';
        $p5->save();

        $p6 = new \App\Models\Point();
        $p6->translateOrNew('en')->title = 'Order';
        $p6->translateOrNew('he')->title = 'הזמנה';
        $p6->color = '#FFFFFF';
        $p6->save();

        $p7 = new \App\Models\Point();
        $p7->translateOrNew('en')->title = 'Pay';
        $p7->translateOrNew('he')->title = 'תשלום';
        $p7->color = '#C81E2C';
        $p7->save();

        $p8 = new \App\Models\Point();
        $p8->translateOrNew('en')->title = 'Sit';
        $p8->translateOrNew('he')->title = 'לשבת';
        $p8->color = '#FFF012';
        $p8->save();

        $p9 = new \App\Models\Point();
        $p9->translateOrNew('en')->title = 'Drink';
        $p9->translateOrNew('he')->title = 'לשתות';
        $p9->color = '#FF4CE4';
        $p9->save();

        $p10 = new \App\Models\Point();
        $p10->translateOrNew('en')->title = 'Pack up';
        $p10->translateOrNew('he')->title = 'לארוז';
        $p10->color = '#27CC6A';
        $p10->save();
    }
}
