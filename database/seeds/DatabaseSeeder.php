<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
         $this->call(JourneysSeeder::class);
         $this->call(PointsSeeder::class);
         $this->call(MediaSeeder::class);
         $this->call(JourneyPointSeeder::class);
         $this->call(AdminsTableSeeder::class);
         $this->call(PasswordsTableSeeder::class);
    }
}
