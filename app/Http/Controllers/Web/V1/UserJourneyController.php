<?php

namespace App\Http\Controllers\Web\V1;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Point;
use App\Models\User;
use App\Models\UserJourney;
use Carbon\CarbonInterval;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class UserJourneyController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(UserJourney $userJourney)
    {
        $periods = $userJourney->touchpoint_periods;

        foreach ($periods as $period){

            if ($period->touchpoint_id){

                $period->load('touchpoint');

                $content = collect([]);

                if ($period->texts()->count()){
                    foreach ($period->texts as $text){
                        $text->type = 'text';
                        $content->push($text);
                    }
                }

                $images = $period->getMedia('image');
                if ($images->count()){
                    foreach ($images as $image){
                        $image->fullUrl = $image->getUrl();
                        $image->type = 'image';
                        $content->push($image);
                    }
                }

                $videos = $period->getMedia('video');
                if ($videos->count()){
                    foreach ($videos as $video){
                        $video->fullUrl = $video->getUrl();
                        $video->type = 'video';
                        $content->push($video);
                    }
                }

                $audios = $period->getMedia('audio');
                if ($audios->count()){
                    foreach ($audios as $audio){
                        $audio->fullUrl = $audio->getUrl();
                        $audio->type = 'audio';
                        $content->push($audio);
                    }
                }

                $period->content = $content->sortBy('created_at')->values()->all();

            }
        }

        return responder()->success($periods);
    }

    public function sendExcel(Request $request, UserJourney $userJourney)
    {
        $periods = $userJourney->touchpoint_periods;

        $user = User::find($request->get('user_id'));
        $title = $userJourney->journey->title;

        $excel = \Excel::create('report_' . $title . '_' . 'journey ' . date("U"), function($excel)  use ($periods, $title, $user) {

            $excel->sheet('Report', function($sheet) use ($periods, $title, $user) {

                // User
                $sheet->cell('A1', function($cell) {$cell->setValue('User: ');});
                $sheet->cell('B1', function($cell) use ($user) {$cell->setValue($user->email);});
                if ($user->name){
                    $sheet->cell('C1', function($cell) use ($user) {$cell->setValue($user->name);});
                }
                if ($user->phone){
                    $sheet->cell('D1', function($cell) use ($user) {$cell->setValue((string) $user->phone);});
                }

                // Table headers

                $sheet->cell('A3', function($cell) {$cell->setValue('Title');});
                $sheet->cell('B3', function($cell) {$cell->setValue('Start');});
                $sheet->cell('C3', function($cell) {$cell->setValue('End');});
                $sheet->cell('D3', function($cell) {$cell->setValue('Duration');});
                $sheet->cell('E3', function($cell) {$cell->setValue('Address');});
                $sheet->cell('F3', function($cell) {$cell->setValue('Videos');});
                $sheet->cell('G3', function($cell) {$cell->setValue('Images');});
                $sheet->cell('H3', function($cell) {$cell->setValue('Audios');});
                $sheet->cell('I3', function($cell) {$cell->setValue('Texts');});
                $sheet->cell('J3', function($cell) {$cell->setValue('Mark');});

                $xlsRow = 4;

                foreach ($periods as $period) {

                    if (!$period->touchpoint_id){
                        $sheet->cell('A'.$xlsRow, function($cell) {$cell->setValue('Touchpoints list');});
                        $sheet->cell('B'.$xlsRow, function($cell) use ($period) {$cell->setValue($period->started_at);});
                        $sheet->cell('C'.$xlsRow, function($cell) use ($period) {$cell->setValue($period->finished_at);});
                        $sheet->cell('D'.$xlsRow, function($cell) use ($period) {
                            $interval = CarbonInterval::seconds($period->duration);
                            $cell->setValue($interval->forHumans());
                        });
                        $sheet->cell('E'.$xlsRow, function($cell) use ($period) {$cell->setValue($period->address);});
                        $xlsRow++;

                    } else {

                        $sheet->cell('A'.$xlsRow, function($cell) use ($period) {$cell->setValue(Point::find($period->touchpoint->point_id)->title);});
                        $sheet->cell('B'.$xlsRow, function($cell) use ($period) {$cell->setValue($period->started_at);});
                        $sheet->cell('C'.$xlsRow, function($cell) use ($period) {$cell->setValue($period->finished_at);});
                        $sheet->cell('D'.$xlsRow, function($cell) use ($period) {
                            $interval = CarbonInterval::seconds($period->duration);
                            $cell->setValue($interval->forHumans());
                        });
                        $sheet->cell('E'.$xlsRow, function($cell) use ($period) {$cell->setValue($period->address);});

                        $videos = $period->getMedia('video');
                        if ($videos->count()){
                            foreach ($videos as $key => $video){
                                $row = $xlsRow + $key;
                                $sheet->cell('F'.$row, function($cell) use ($video) {$cell->setValue($video->getUrl());});
                            }
                        }

                        $images = $period->getMedia('image');
                        if ($images->count()){
                            foreach ($images as $key => $image){
                                $row = $xlsRow + $key;
                                $sheet->cell('G'.$row, function($cell) use ($image) {$cell->setValue($image->getUrl());});
                            }
                        }

                        $audios = $period->getMedia('audio');
                        if ($audios->count()){
                            foreach ($audios as $key => $audio){
                                $row = $xlsRow + $key;
                                $sheet->cell('H'.$row, function($cell) use ($audio) {$cell->setValue($audio->getUrl());});
                            }
                        }

                        if ($period->texts->count()){
                            foreach ($period->touchpoint->texts as $key => $text){
                                $row = $xlsRow + $key;
                                $sheet->cell('I'.$row, function($cell) use ($text) {$cell->setValue($text->content);});
                            }
                        }

                        $sheet->cell('J'.$xlsRow, function($cell) use ($period) {$cell->setValue($period->touchpoint->mark);});

                        $max = max($videos->count(), $images->count(), $audios->count(), $period->texts->count());

                        $xlsRow = $xlsRow + $max;

                    }

                }

            });

        })->store('xls', storage_path('reports'), true);

        $filenames[] = $excel['full'];
        $admin = Admin::find($request->get('admin_id'));

        Mail::send('emails.report', [], function($message) use ($filenames, $admin) {

            $message->to($admin->email)->subject('Report');

            foreach ($filenames as $file){
                $message->attach($file);
            }

        });


    }
}
