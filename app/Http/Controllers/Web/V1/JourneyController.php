<?php

namespace App\Http\Controllers\Web\V1;

use App\Http\Controllers\Controller;
use App\Models\Journey;
use Illuminate\Http\Request;

class JourneyController extends Controller
{
    /**
     * Display a listing of the journeys.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $journeys = Journey::query()->with(['points' => function($q) {
            $q->orderBy('pivot_weight', 'asc')->whereNull('points.user_id');
        }])->get();
        foreach ($journeys as $journey){
            $journey->getTranslationsArray();
            foreach ($journey->translations as $tr){
                if ($tr->locale === 'en'){
                    $journey->title_en = $tr->title;
                }
                if ($tr->locale === 'he'){
                    $journey->title_he = $tr->title;
                }
            }
        }

        return responder()->success($journeys);

    }

    /**
     * Binds user to journey.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // TODO: request

        $journey = new Journey();
        $journey->translateOrNew('en')->title = $request->get('title_en');
        $journey->translateOrNew('he')->title = $request->get('title_he');
        $journey->save();

        $points = $request->get('points');

        foreach ($points as $point){
            $journey->points()->attach($point['id'], ['created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()]);
        }

        return responder()->success($journey);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Journey $journey)
    {

        $journey->load(['points' => function($q) {
            $q->orderBy('pivot_weight', 'asc')->whereNull('points.user_id');
        }]);

        $journey->getTranslationsArray();

        foreach ($journey->translations as $tr){
            if ($tr->locale === 'en'){
                $journey->title_en = $tr->title;
            }
            if ($tr->locale === 'he'){
                $journey->title_he = $tr->title;
            }
        }

        return responder()->success($journey);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Journey $journey)
    {
        if ($request->has('order')) {

            $map = collect($request->get('order'))->mapWithKeys(function($item, $index) {
                return [$item => [
                    'weight' => $index,
                ]];
            });

            $journey
                ->points()
                ->wherePivot('user_id', '=', null)
                ->sync($map, true);

            return responder()->success();
        }

        $journey->translateOrNew('en')->title = $request->get('title_en');
        $journey->translateOrNew('he')->title = $request->get('title_he');
        $journey->save();

        $points = $request->get('points');

        $original_points = collect($journey->points);

        $max_weight = $original_points->max(function($item) {
            return $item->pivot->weight;
        });

        $map = collect($points)->mapWithKeys(function($item) use ($original_points, &$max_weight) {

            $original_point = $original_points->firstWhere('id', '=', $item['id']);

            if (!is_null($original_point)) {
                return [$item['id'] => [
                    'weight' => $original_point->pivot->weight
                ]];
            } else {

                return [$item['id'] => [
                    'weight' => ++$max_weight,
                ]];
            }
        });

        $result = $journey
            ->points()
            ->wherePivot('user_id', '=', null)
            ->sync($map, true);

        $journey
            ->user_weighted_points()
            ->detach($result['detached']);

        if ($result['attached'] > 0) {

            $users = $journey->user_weighted_points->pluck('pivot.user_id')->unique();

            foreach ($users as $user) {

                $max_weight = $journey
                    ->user_weighted_points()
                    ->wherePivot('user_id', '=', $user)
                    ->max('journey_point.weight');

                $map = collect($result['attached'])->mapWithKeys(function ($item) use ($max_weight, $user) {
                    return [
                        $item => [
                            'user_id' => $user,
                            'weight' => $max_weight + 1,
                        ]];
                });

                $journey
                    ->user_weighted_points()
                    ->sync($map, false);
            }
        }

        return responder()->success();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Journey $journey)
    {
        $journey->delete();
        return responder()->success();
    }
}
