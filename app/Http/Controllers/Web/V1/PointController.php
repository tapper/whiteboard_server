<?php

namespace App\Http\Controllers\Web\V1;

use App\Models\Journey;
use App\Models\Point;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class PointController extends Controller
{
    /**
     * Display a listing of the points.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $points = Point::whereNull('user_id')->get();
        foreach ($points as $point) {
            $point->getTranslationsArray();
            foreach ($point->translations as $tr){
                if ($tr->locale === 'en'){
                    $point->title_en = $tr->title;
                }
                if ($tr->locale === 'he'){
                    $point->title_he = $tr->title;
                }
            }
        }
        return responder()->success($points->sortBy('weight')->values()->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $point = new Point();
        $point->translateOrNew('en')->title = $request->get('title_en');
        $point->translateOrNew('he')->title = $request->get('title_he');
        $point->color = $request->get('color');
        $point->save();

        return responder()->success();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Point $point)
    {
        if ($request->has('title_en'))
        {
            $point->translateOrNew('en')->title = $request->get('title_en');
        }

        if ($request->has('title_he'))
        {
            $point->translateOrNew('he')->title = $request->get('title_he');
        }

        if ($request->has('color'))
        {
            $point->color = $request->get('color');
        }

        $point->save();

        return responder()->success();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Point $point)
    {
        DB::table('journey_point')->where('point_id', $point->id)->delete();

        $point->delete();
        return responder()->success();
    }
}
