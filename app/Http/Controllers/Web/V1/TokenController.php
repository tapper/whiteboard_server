<?php

namespace App\Http\Controllers\Web\V1;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Password;
use Illuminate\Http\Request;

class TokenController extends Controller
{
    public function login(Request $request)
    {

        $admin = Admin::where('username', $request->get('username'))->where('password', $request->get('password'))->first();

        if (!$admin){
            return responder()->error('login_again')->respond();
        }

        return responder()->success($admin, function ($admin) {
            return ['id' => $admin->id];
        })->respond();
    }

    public function show(Admin $admin)
    {
        return responder()->success($admin);
    }

    public function update(Request $request, Admin $admin)
    {
        $admin->username = $request->get('username');
        $admin->password = $request->get('password');
        $admin->email = $request->get('email');
        $admin->save();

        return responder()->success();
    }

    public function showPassword()
    {
        $password = Password::orderBy('id', 'desc')->first();

        return responder()->success($password);
    }


    public function storePassword(Request $request)
    {
        $password = new Password();
        $password->password = $request->get('password');
        $password->save();

        return responder()->success();
    }
}
