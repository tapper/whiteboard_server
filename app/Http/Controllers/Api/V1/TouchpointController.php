<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Journey;
use App\Models\Point;
use App\Models\Touchpoint;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TouchpointController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // TODO: request

        $touchpoint = new Touchpoint();
        $touchpoint->fill($request->all());
        $touchpoint->user_journey_id = $request->get('user_journey_id');
        $touchpoint->point_id = $request->get('point_id');
        $touchpoint->save();

        return responder()->success($touchpoint);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Touchpoint $touchpoint)
    {
        $touchpoint->mark = $request->get('mark');
        $touchpoint->save();

        return responder()->success();
    }

}
