<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Password;
use App\Models\User;
use Illuminate\Http\Request;

class TokenController extends Controller
{
    public function login(Request $request)
    {

        $password = Password::orderBy('id', 'desc')->first();

        if ($request->get('password') != $password->password){
            return responder()->error('login_again')->respond();
        }

        $user = User::whereEmail($request->get('email'))->first();

        if (!$user){
            $user = new User();
            $user->email = $request->get('email');
            $user->language = 'en';
        }

        $user->api_token = str_random(60);
        $user->save();

        return responder()->success($user, function ($user) {
            return ['id' => $user->id, 'api_token' => $user->api_token, 'language' => $user->language];
        })->respond();
    }

    public function changeLanguage(Request $request)
    {
        $user = auth()->guard('api')->user();

        if (!$user){
            return responder()->success();
        }

        $user->language = $request->get('language');
        $user->save();

        return responder()->success();
    }
}
