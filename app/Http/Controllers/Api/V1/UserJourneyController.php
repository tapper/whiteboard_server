<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Journey;
use App\Models\JourneyPeriod;
use App\Models\Touchpoint;
use App\Models\TouchpointPeriod;
use App\Models\UserJourney;
use Carbon\Carbon;
use Geocoder\Laravel\Facades\Geocoder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserJourneyController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->guard('api')->user();
        $journey = Journey::find($request->get('journey_id'));

        $user_journey = new UserJourney();
        $user_journey->user_id = $user->id;
        $user_journey->journey_id = $journey->id;
        $user_journey->save();

        return responder()->success($user_journey);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(UserJourney $userJourney)
    {
        $user = auth()->guard('api')->user();
        app()->setLocale($user->language);
        return responder()->success($userJourney);
    }

    public function reportTouchpointPeriod(Request $request, UserJourney $userJourney)
    {
        if ($request->get('time') === 'start'){

            $period = new TouchpointPeriod();

            $period->user_journey()->associate($userJourney);

            if ($request->get('touchpoint_id')){
                $period->touchpoint()->associate(Touchpoint::find($request->get('touchpoint_id')));
            }

            $period->started_at = Carbon::now();
            $period->lat = $request->get('lat');
            $period->lng = $request->get('lng');

            $address = Geocoder::reverse($request->get('lat'), $request->get('lng'))->get();
            if ($address->count()){
                $period->address = $address->first()->getFormattedAddress();
            }
            $period->save();

        } else {

            $period = TouchpointPeriod::where('user_journey_id', $userJourney->id)->orderBy('started_at', 'DESC')->first();

            $period->finished_at = Carbon::now();
            $period->duration = Carbon::createFromFormat('Y-m-d H:i:s', $period->started_at)->diffInSeconds($period->finished_at);

            $period->save();

            if ($request->get('next')){

                $new_period = new TouchpointPeriod();

                $new_period->user_journey()->associate($userJourney);
                $new_period->started_at = Carbon::now();
                $new_period->lat = $request->get('lat');
                $new_period->lng = $request->get('lng');

                $address = Geocoder::reverse($request->get('lat'), $request->get('lng'))->get();
                if ($address->count()){
                    $new_period->address = $address->first()->getFormattedAddress();
                }

                if ($request->get('touchpoint_id')){
                    $new_period->touchpoint()->associate(Touchpoint::find($request->get('touchpoint_id')));
                }

                $new_period->save();

            }

        }


        return responder()->success();
    }

    public function reportJourneyPeriod(Request $request, UserJourney $userJourney)
    {
        if ($request->get('time') === 'start'){

            $period = new JourneyPeriod();
            $period->user_journey()->associate($userJourney);
            $period->started_at = Carbon::now();
            $period->lat_start = $request->get('lat');
            $period->lng_start = $request->get('lng');

            $address = Geocoder::reverse($request->get('lat'), $request->get('lng'))->get();
            if ($address->count()){
                $period->address_start = $address->first()->getFormattedAddress();
            }

            $period->save();

        } else {

            $period = JourneyPeriod::where('user_journey_id', $userJourney->id)->whereNull('finished_at')->orderBy('started_at', 'DESC')->first();
            $period->finished_at = Carbon::now();
            $period->lat_end = $request->get('lat');
            $period->lng_end = $request->get('lng');

            $address = Geocoder::reverse($request->get('lat'), $request->get('lng'))->get();
            if ($address->count()){
                $period->address_end = $address->first()->getFormattedAddress();
            }

            $period->save();
        }

        return responder()->success();
    }

}
