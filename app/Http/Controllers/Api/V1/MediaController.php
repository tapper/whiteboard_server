<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Touchpoint;
use App\Models\TouchpointPeriod;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Plank\Mediable\Media;
use Plank\Mediable\MediaUploaderFacade;

class MediaController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Touchpoint $touchpoint)
    {
        $unix = time();

        \Log::info(json_encode($request->all()));

        $name = "{$request->get('type')}_touchpoint_{$touchpoint->id}_{$unix}";

        $media = MediaUploaderFacade::fromSource($request->file('file'))->useFilename($name)->upload();

        $touchpoint->attachMedia($media, $request->get('type'));

        $touchpoint_period = TouchpointPeriod::where('touchpoint_id', $touchpoint->id)
                                ->where('user_journey_id', $touchpoint->user_journey->id)
                                ->orderBy('id', 'DESC')
                                ->first();

        $touchpoint_period->attachMedia($media, $request->get('type'));

        $touchpoint->loadMedia();

        return responder()->success($touchpoint);

    }

    public function storeAvatar(Request $request)
    {
        $unix = time();

        $user = User::find($request->get('user_id'));

        $name = "image_user_{$user->id}_{$unix}";

        $media = MediaUploaderFacade::fromSource($request->file('file'))->useFilename($name)->upload();

        $user->syncMedia($media, 'avatar');

        return responder()->success();
    }

    public function deleteAvatar($id)
    {
        $file = Media::find($id);
        $file->delete();

        return responder()->success();
    }

}
