<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserJourney extends Model
{
    protected $with = ['touchpoints', 'journey_periods'];

    protected $appends = ['title'];

    public function touchpoints()
    {
        return $this->hasMany(Touchpoint::class);
    }

    public function touchpoint_periods()
    {
        return $this->hasMany(TouchpointPeriod::class);
    }

    public function journey_periods()
    {
        return $this->hasMany(JourneyPeriod::class);
    }

    public function users()
    {
        return $this->belongsTo(User::class);
    }

    public function journey()
    {
        return $this->belongsTo(Journey::class);
    }

    public function getTitleAttribute()
    {
        $title = Journey::find($this->journey_id);

        if ($title){
            return $title->title;
        }

        return '';
    }
}
