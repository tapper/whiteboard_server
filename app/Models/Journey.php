<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;

class Journey extends Model
{
    use \Dimsav\Translatable\Translatable, Mediable;

    public $translatedAttributes = ['title'];

    protected $appends = ['image'];

    public function points()
    {
        return $this
            ->belongsToMany(Point::class)
            ->withPivot('user_id', 'weight')
            ->wherePivot('user_id', null)
            ->withTimestamps();
    }

    public function my_weighted_points()
    {
        return $this
            ->belongsToMany(Point::class)
            ->withPivot('user_id', 'weight')
            ->wherePivot('user_id', auth()->user()->id)
            ->withTimestamps();
    }

    public function user_weighted_points()
    {
        return $this
            ->belongsToMany(Point::class)
            ->withPivot('user_id', 'weight')
            ->wherePivot('user_id', '<>', null)
            ->withTimestamps();
    }

    public function user_journeys()
    {
        return $this->hasMany(UserJourney::class);
    }

    public function getImageAttribute()
    {
        $journey = $this->getMedia('image');
        if ($journey->count() > 0){
            return $journey[0]->getUrl();
        }
        return '';
    }

    public function media()
    {
        return $this->morphToMany(config('mediable.model'), 'mediable')
            ->withPivot('tag', 'order', 'mediable_type')
            ->orderBy('order');
    }
}
