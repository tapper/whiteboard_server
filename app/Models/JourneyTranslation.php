<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JourneyTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['title'];
}
