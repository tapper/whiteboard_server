<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Text extends Model
{

    protected $fillable = ['content'];

    public function touchpoint()
    {
        return $this->belongsTo(Touchpoint::class);
    }

    public function touchpoint_period()
    {
        return $this->belongsTo(TouchpointPeriod::class);
    }
}
