<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Plank\Mediable\Mediable;

class User extends Authenticatable
{
    use Notifiable, Mediable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'language'
    ];

    protected $appends = ['avatar'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function points()
    {
        return $this->hasMany(Point::class);
    }

    public function user_journeys()
    {
        return $this->hasMany(UserJourney::class);
    }

    public function getAvatarAttribute()
    {
        $avatar = $this->getMedia('avatar');

        if ($avatar->count() > 0)
            return $avatar[0]->getUrl();

        return '';
    }

}
