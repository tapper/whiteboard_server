<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    use \Dimsav\Translatable\Translatable;

    public $translatedAttributes = ['title'];

    protected $fillable = ['color', 'weight'];

    protected $appends = ['itemName'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function journeys()
    {
        return $this
            ->belongsToMany(Journey::class)
            ->withPivot('user_id', 'weight')
            ->wherePivot('user_id', null)
            ->withTimestamps();
    }

    public function user_weighted_journeys()
    {
        return $this
            ->belongsToMany(Journey::class)
            ->withPivot('user_id', 'weight')
            ->wherePivot('user_id', auth()->user()->id)
            ->withTimestamps();
    }

    public function getItemNameAttribute()
    {
        return $this->title;
    }

}
